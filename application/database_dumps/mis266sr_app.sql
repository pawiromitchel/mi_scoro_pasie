-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 08, 2015 at 07:16 AM
-- Server version: 5.6.23
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mis266sr_app`
--
CREATE DATABASE IF NOT EXISTS `mis266sr_app` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `mis266sr_app`;

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE IF NOT EXISTS `ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `uploaded_img` text NOT NULL,
  `link` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `organisations`
--

CREATE TABLE IF NOT EXISTS `organisations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `organisations`
--

INSERT INTO `organisations` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'msp', '2015-10-02 11:34:12', '0000-00-00 00:00:00'),
(2, 'bos', '2015-10-02 11:34:12', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `organisation_posts`
--

CREATE TABLE IF NOT EXISTS `organisation_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organisation_id` int(11) NOT NULL,
  `post_category_id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` varchar(255) NOT NULL,
  `privacy` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `post_category`
--

CREATE TABLE IF NOT EXISTS `post_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `post_category`
--

INSERT INTO `post_category` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'biografie', '2015-10-02 11:32:11', '0000-00-00 00:00:00'),
(2, ' schoolvervoer', '2015-10-02 11:32:11', '0000-00-00 00:00:00'),
(3, 'schoolregelement', '2015-10-02 11:32:11', '0000-00-00 00:00:00'),
(5, 'about', '2015-10-02 11:32:11', '0000-00-00 00:00:00'),
(6, 'nieuws', '2015-10-02 11:32:11', '0000-00-00 00:00:00'),
(7, 'faq', '2015-10-02 11:32:11', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

CREATE TABLE IF NOT EXISTS `schools` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `schools`
--

INSERT INTO `schools` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'natin coppename', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'natin nickerie', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'natin leysweg', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'lyceum 1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'lyceum 2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'imeao 1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'imeao 2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'imeao 3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'havo 1', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'havo 2', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'havo 3', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `school_courses`
--

CREATE TABLE IF NOT EXISTS `school_courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `school_events`
--

CREATE TABLE IF NOT EXISTS `school_events` (
  `id` int(255) NOT NULL,
  `school_id` int(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `school_exams`
--

CREATE TABLE IF NOT EXISTS `school_exams` (
  `id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `subject` text NOT NULL,
  `date` datetime NOT NULL,
  `destination` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `school_posts`
--

CREATE TABLE IF NOT EXISTS `school_posts` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `school_id` varchar(255) NOT NULL,
  `post_category_id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` varchar(255) NOT NULL,
  `privacy` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` datetime NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `school_posts`
--

INSERT INTO `school_posts` (`id`, `school_id`, `post_category_id`, `subject`, `body`, `privacy`, `created_at`, `updated_at`, `active`) VALUES
(1, '1', 6, 'I ben een aap', 'ik ben harig', 'public', '0000-00-00', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `school_schedule`
--

CREATE TABLE IF NOT EXISTS `school_schedule` (
  `id` int(255) NOT NULL,
  `school_id` int(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `body` varchar(255) NOT NULL,
  `uploaded_pdf` varchar(255) NOT NULL,
  `privacy` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `school_courses_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `birth_date` datetime NOT NULL,
  `tel_number` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_type_id`, `school_id`, `school_courses_id`, `first_name`, `last_name`, `email`, `gender`, `username`, `password`, `birth_date`, `tel_number`, `created_at`, `updated_at`, `active`) VALUES
(1, 3, 1, 0, 'Mitchel', 'Pawirodinomo', 'pawiromitchel@hotmail.com', 'Male', 'mitchel', '4144928ca85889c3495cced0bd84c141c2fe42ed', '2015-10-01 00:00:00', '8781953', '2015-10-07 12:27:13', '0000-00-00 00:00:00', 1),
(2, 3, 1, 0, 'Timothy', 'Pocorni', 'Pocorni@hotmail.com', 'Female', 'timmy', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '0000-00-00 00:00:00', '', '2015-10-07 12:31:41', '0000-00-00 00:00:00', 1),
(3, 3, 1, 0, 'Jonathan', 'Tobi', 'Jonathan', 'Female', 'jonna', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', '0000-00-00 00:00:00', '', '2015-10-07 12:31:43', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE IF NOT EXISTS `user_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`id`, `name`, `date_created`, `date_updated`) VALUES
(1, 'bos', '2015-10-07 12:25:12', '0000-00-00 00:00:00'),
(2, 'msp', '2015-10-07 12:25:12', '0000-00-00 00:00:00'),
(3, 'school', '2015-10-07 12:25:12', '0000-00-00 00:00:00'),
(4, 'student', '2015-10-07 12:34:04', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
