-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Dec 05, 2015 at 02:33 PM
-- Server version: 5.6.27
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mis266sr_msp_db`
--
CREATE DATABASE IF NOT EXISTS `mis266sr_msp_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `mis266sr_msp_db`;

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

DROP TABLE IF EXISTS `ads`;
CREATE TABLE IF NOT EXISTS `ads` (
  `ad_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(512) DEFAULT NULL,
  `img_id` int(11) DEFAULT NULL,
  `link` varchar(512) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ad_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`ad_id`, `title`, `description`, `img_id`, `link`, `created_at`, `updated_at`, `active`) VALUES
(1, 'SEMC Hardware Aktie', 'Uitverkoop ISUZU D-Max met 93% korting', NULL, 'semc.sr', '2015-11-21 18:08:13', '2015-11-21 19:06:35', 1),
(2, 'New', 'Test', NULL, 'link.l', '2015-11-21 19:53:12', NULL, 0),
(3, 'sfsd', 'sdfds', NULL, 'sdfds', '2015-11-21 19:55:07', NULL, 0),
(4, 'Test 1', 'Door cher', NULL, 'https://www.facebook.com/', '2015-12-03 00:58:50', '2015-12-03 00:59:03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bos_contact`
--

DROP TABLE IF EXISTS `bos_contact`;
CREATE TABLE IF NOT EXISTS `bos_contact` (
  `bos_contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `number_1` varchar(255) DEFAULT NULL,
  `number_2` varchar(255) DEFAULT NULL,
  `number_3` varchar(45) NOT NULL,
  `website` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `locatie` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`bos_contact_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `bos_contact`
--

INSERT INTO `bos_contact` (`bos_contact_id`, `address`, `number_1`, `number_2`, `number_3`, `website`, `email`, `locatie`, `created_at`, `updated_at`, `active`) VALUES
(1, 'Jessurunstraat # 15', '+597-476005', '+597-473478', '+597-474255', 'http://www.bos-suriname.com', 'bos@education.gov.sr', 'Paramaribo, Suriname', '2015-12-03 00:46:17', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bos_information`
--

DROP TABLE IF EXISTS `bos_information`;
CREATE TABLE IF NOT EXISTS `bos_information` (
  `bos_information_id` int(11) NOT NULL AUTO_INCREMENT,
  `st_voorwaarde` varchar(1024) NOT NULL,
  `sb_voorwaarde` varchar(1024) NOT NULL,
  `sv_voorwaarde` varchar(1024) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`bos_information_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `bos_information`
--

INSERT INTO `bos_information` (`bos_information_id`, `st_voorwaarde`, `sb_voorwaarde`, `sv_voorwaarde`, `created_at`, `updated_at`) VALUES
(1, '<p>Studie Toelage Voorwaarde1</p>', '<p>Studie Beurs Voorwaarde 2</p>', 'School Vervoer Voorwaarde', '2015-11-21 16:50:04', '2015-11-21 16:50:04');

-- --------------------------------------------------------

--
-- Table structure for table `bos_posts`
--

DROP TABLE IF EXISTS `bos_posts`;
CREATE TABLE IF NOT EXISTS `bos_posts` (
  `bos_post_id` int(11) NOT NULL AUTO_INCREMENT,
  `author` int(11) DEFAULT NULL,
  `post_type` varchar(45) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `topic` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `img_id` int(11) DEFAULT NULL,
  `privacy` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`bos_post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `bos_posts`
--

INSERT INTO `bos_posts` (`bos_post_id`, `author`, `post_type`, `category`, `topic`, `description`, `img_id`, `privacy`, `created_at`, `updated_at`, `active`) VALUES
(1, 3, '', 'nieuws', 'Bos test', '<p>Bost post content here</p>', NULL, 'public', '2015-12-02 19:44:15', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
CREATE TABLE IF NOT EXISTS `courses` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `uploaded_pdf` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `img`
--

DROP TABLE IF EXISTS `img`;
CREATE TABLE IF NOT EXISTS `img` (
  `img_id` int(11) NOT NULL AUTO_INCREMENT,
  `school_post_id` int(11) DEFAULT NULL,
  `school_event_id` int(11) DEFAULT NULL,
  `msp_post_id` int(11) DEFAULT NULL,
  `ad_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`img_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `img_slides`
--

DROP TABLE IF EXISTS `img_slides`;
CREATE TABLE IF NOT EXISTS `img_slides` (
  `img_slide_id` int(11) NOT NULL AUTO_INCREMENT,
  `organisation` varchar(45) DEFAULT NULL,
  `school_id` int(11) DEFAULT NULL,
  `img_src` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`img_slide_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `msp_about`
--

DROP TABLE IF EXISTS `msp_about`;
CREATE TABLE IF NOT EXISTS `msp_about` (
  `msp_about_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL,
  PRIMARY KEY (`msp_about_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `msp_faq`
--

DROP TABLE IF EXISTS `msp_faq`;
CREATE TABLE IF NOT EXISTS `msp_faq` (
  `msp_faq_id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL,
  PRIMARY KEY (`msp_faq_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `msp_posts`
--

DROP TABLE IF EXISTS `msp_posts`;
CREATE TABLE IF NOT EXISTS `msp_posts` (
  `msp_post_id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` int(11) DEFAULT NULL,
  `author` int(11) DEFAULT NULL,
  `post_type` varchar(45) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `topic` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `img_id` int(11) DEFAULT NULL,
  `privacy` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`msp_post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `msp_posts`
--

INSERT INTO `msp_posts` (`msp_post_id`, `school_id`, `author`, `post_type`, `category`, `topic`, `description`, `img_id`, `privacy`, `created_at`, `updated_at`, `active`) VALUES
(1, 0, 2, '', 'nieuws', 'MSP Mobile Applicatie', 'Lancering van de MSP mobile app. Dit is bestemd voor alle scholen in Suriname.', NULL, 'public', NULL, '2015-12-03 01:45:55', 1),
(4, NULL, 5, NULL, 'nieuws', 'sdfsd', 'sfd', NULL, 'private', '2015-11-21 18:20:48', NULL, 1),
(5, NULL, 5, NULL, 'nieuws', 'MSP News test', 'Dit is de text. Dit is tim nu aan het testen', NULL, 'private', '2015-12-03 00:34:42', NULL, 1),
(6, NULL, 5, NULL, 'nieuws', 'Test 1', 'Door cher', NULL, 'public', '2015-12-03 00:59:42', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `repetities`
--

DROP TABLE IF EXISTS `repetities`;
CREATE TABLE IF NOT EXISTS `repetities` (
  `repetitie_id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` int(11) NOT NULL,
  `author` varchar(45) DEFAULT NULL,
  `klas` varchar(45) DEFAULT NULL,
  `uploaded_pdf` varchar(255) DEFAULT NULL,
  `privacy` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`repetitie_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `roosters`
--

DROP TABLE IF EXISTS `roosters`;
CREATE TABLE IF NOT EXISTS `roosters` (
  `rooster_id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` int(11) NOT NULL,
  `author` varchar(45) DEFAULT NULL,
  `class` varchar(45) DEFAULT NULL,
  `uploaded_pdf` varchar(255) DEFAULT NULL,
  `privacy` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rooster_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `schoolreglement`
--

DROP TABLE IF EXISTS `schoolreglement`;
CREATE TABLE IF NOT EXISTS `schoolreglement` (
  `schoolreglement_id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(1024) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL,
  PRIMARY KEY (`schoolreglement_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `schoolreglement`
--

INSERT INTO `schoolreglement` (`schoolreglement_id`, `school_id`, `title`, `description`, `created_at`, `updated_at`, `active`) VALUES
(1, 1, NULL, '<p>Reglement van NATIN-MBO</p>', '2015-12-02 18:50:36', '2015-12-02 18:50:36', 0);

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

DROP TABLE IF EXISTS `schools`;
CREATE TABLE IF NOT EXISTS `schools` (
  `school_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `visie` varchar(1024) NOT NULL,
  `missie` varchar(1024) NOT NULL,
  `organisatie` varchar(1024) NOT NULL,
  `school_code` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`school_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `schools`
--

INSERT INTO `schools` (`school_id`, `name`, `description`, `logo`, `visie`, `missie`, `organisatie`, `school_code`, `created_at`, `updated_at`, `active`) VALUES
(1, 'NATIN-MBO', 'Natuur Technisch Instituut Suriname', 'http://miskoropasie.sr/msp_api/uploads/logo/NATIN-MBO Logo 420x420.png', '<p>afdaf</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum mauris. Maecenas congue ligula ac quam viverra nec consectetur ante hendrerit. Donec et mollis dolor. Praesent et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia consectetur. Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ut gravida lorem. Ut turpis felis, pulvinar a semper sed, adipiscing id dolor. Pellentesque auctor nisi id magna consequat sagittis. Curabitur dapibus enim sit amet elit pharetra tincidunt feugiat nisl imperdiet. Ut convallis libero in urna ultrices accumsan. Donec sed odio eros. Donec viverra mi quis quam pulvinar at malesuada arcu rhoncus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In rutrum accumsan ultricies. Mauris vitae nisi at sem facilisis semper ac in est.\r\n\r\n\r\n                  Vivamus fermentum semper porta. Nun', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum mauris. Maecenas congue ligula ac quam viverra nec consectetur ante hendrerit. Donec et mollis dolor. Praesent et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia consectetur. Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ut gravida lorem. Ut turpis felis, pulvinar a semper sed, adipiscing id dolor. Pellentesque auctor nisi id magna consequat sagittis. Curabitur dapibus enim sit amet elit pharetra tincidunt feugiat nisl imperdiet. Ut convallis libero in urna ultrices accumsan. Donec sed odio eros. Donec viverra mi quis quam pulvinar at malesuada arcu rhoncus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In rutrum accumsan ultricies. Mauris vitae nisi at sem facilisis semper ac in est.\r\n\r\n\r\n                  Vivamus fermentum semper porta. Nun', 1463830, '2015-11-13 16:20:58', '2015-12-03 20:05:05', 1),
(2, 'HAVO 1', 'Openbaar   Atheneum (HAVO-1)', 'http://miskoropasie.sr/msp_api/uploads/logo/havo1.png', '', '<p>Missie van HAVO I</p>', '0', 1233, '2015-11-13 16:20:58', '2015-12-03 23:51:35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `school_contact`
--

DROP TABLE IF EXISTS `school_contact`;
CREATE TABLE IF NOT EXISTS `school_contact` (
  `school_contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `number_1` varchar(255) DEFAULT NULL,
  `number_2` varchar(255) DEFAULT NULL,
  `number_3` varchar(255) DEFAULT NULL,
  `fax` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `locatie` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` int(11) NOT NULL,
  PRIMARY KEY (`school_contact_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `school_contact`
--

INSERT INTO `school_contact` (`school_contact_id`, `school_id`, `address`, `number_1`, `number_2`, `number_3`, `fax`, `website`, `email`, `locatie`, `created_at`, `updated_at`, `active`) VALUES
(1, 1, 'Coppenamestraat #20', '4444', '131', '123456', '555551', 'natin.sr', 'example@natin.sr', 'Paramaribo, Suriname', '2015-11-30 20:07:06', '2015-11-30 20:07:06', 1),
(2, 2, 'dfsd', 'sfd', '', '', '', '', '', '', '2015-12-01 18:53:46', '2015-12-01 18:53:46', 0);

-- --------------------------------------------------------

--
-- Table structure for table `school_events`
--

DROP TABLE IF EXISTS `school_events`;
CREATE TABLE IF NOT EXISTS `school_events` (
  `school_event_id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` int(11) NOT NULL,
  `author` varchar(45) DEFAULT NULL,
  `topic` varchar(45) DEFAULT NULL,
  `img_id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `privacy` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`school_event_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `school_events`
--

INSERT INTO `school_events` (`school_event_id`, `school_id`, `author`, `topic`, `img_id`, `description`, `location`, `date`, `privacy`, `created_at`, `updated_at`, `active`) VALUES
(1, 1, '1', 'Srefidensi', NULL, 'Optredens van de meest populaire artiesten in Suriname', 'Parbo', NULL, NULL, NULL, NULL, 1),
(2, 1, '', '', NULL, '', '', '0000-00-00', '', '2015-11-18 20:11:01', NULL, 1),
(3, 1, '', '', NULL, '', '', '0000-00-00', '', '2015-11-18 20:11:17', NULL, 1),
(4, 1, '1', 'Yeah', NULL, 'oh yeah', '', '0000-00-00', 'public', '2015-11-18 20:11:54', NULL, 0),
(5, 1, '1', 'sfsd', NULL, 'sfsdf', '', '0000-00-00', 'private', '2015-11-18 20:13:20', NULL, 0),
(6, 1, '', '', NULL, '', '', '0000-00-00', '', '2015-11-18 20:13:52', '2015-12-04 10:37:22', 1),
(7, 1, '1', 'asfds', NULL, '<p>sf</p>', '', '0000-00-00', 'private', '2015-12-01 12:59:34', NULL, 0),
(8, 1, '1', 'asfds', NULL, '<p>sf</p>', '', '0000-00-00', 'private', '2015-12-01 13:00:35', NULL, 0),
(9, 1, '1', 'testing this nw', NULL, '<p>dfda</p>', '', '0000-00-00', 'private', '2015-12-01 13:04:22', NULL, 0),
(10, 2, '4', 'sdf', NULL, '<p>dgsdgf</p>', '', '0000-00-00', 'private', '2015-12-01 19:01:33', NULL, 1),
(11, 1, '1', 'new event', NULL, '<p>no content</p>', '', '0000-00-00', 'public', '2015-12-04 00:25:12', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `school_posts`
--

DROP TABLE IF EXISTS `school_posts`;
CREATE TABLE IF NOT EXISTS `school_posts` (
  `school_post_id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` int(11) DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `post_type` varchar(45) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `topic` varchar(45) DEFAULT NULL,
  `img_id` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `privacy` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`school_post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `school_posts`
--

INSERT INTO `school_posts` (`school_post_id`, `school_id`, `author`, `post_type`, `category`, `topic`, `img_id`, `description`, `privacy`, `created_at`, `updated_at`, `active`) VALUES
(1, 1, '1', '', 'nieuws', 'Her-inschijving', NULL, 'Her inschrijving is deze dag', 'public', NULL, '2015-11-17 21:44:39', 1),
(2, 1, '1', '', 'nieuws', 'fdfmgpm', NULL, 'Yo', 'public', '2015-11-18 17:57:32', '2015-11-18 19:27:30', 0),
(3, 1, '1', '', 'nieuws', 'dsfdsfds', NULL, '<p>Uhm gewoon een test text hier</p>', 'public', '2015-11-18 20:12:49', '2015-11-30 22:52:41', 1),
(4, 2, '4', '', 'nieuws', 'dfgfd', NULL, '<p>dfgfd</p>', 'private', '2015-12-01 18:34:20', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `school_id` int(11) DEFAULT NULL,
  `user_type` varchar(45) NOT NULL,
  `branch` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `schoolyear` varchar(255) NOT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `birth_date` date NOT NULL,
  `number` varchar(45) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `user_created_at` timestamp NULL DEFAULT NULL,
  `user_updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `school_id`, `user_type`, `branch`, `class`, `schoolyear`, `firstname`, `lastname`, `email`, `username`, `password`, `gender`, `birth_date`, `number`, `active`, `user_created_at`, `user_updated_at`) VALUES
(1, 1, 'school', '', '', '', 'Timothy', 'Pocorni', 'tim.poco01@gmail.com', 'timmy', '37a72090e1a30a7080c36883b0aac0bd5ef51690', '', '0000-00-00', '', 1, NULL, '2015-12-05 03:03:03'),
(2, 1, 'student', '', '', '', 'Mitchel', 'Pawirodinomo', 'pawiromitchel@hotmail.com', 'mitchel', '48293ec047687fd5d29089abec85758123a75201', 'male', '0000-00-00', NULL, 1, NULL, NULL),
(3, NULL, 'bos', '', '', '', 'Agon', 'Emmanuel', 'agon28@live.com', 'agon', '7b22a1b7faa05a7a76af9d3646a09ba60bf0c73c', 'male', '0000-00-00', NULL, 1, NULL, NULL),
(4, 2, 'school', '', '', '', 'Jonathan', 'Tobi', 'tobi@live.com', 'jonna', '35e5e3787f8e35ae56688865ca891de7e3e35b10', 'male', '0000-00-00', NULL, 1, NULL, NULL),
(5, NULL, 'msp', '', '', '', 'Cher', 'Dwarkasing', 'cher@gmail.com', 'cher', '6342e8b8c1ca9166fc8f10fbb225ae9fa9c516ca', '', '0000-00-00', '', 1, NULL, '2015-12-05 03:12:53'),
(11, 1, 'student', 'test', '1.35', '2015', 'isiah', 'pawirodinomo', 'pawiroisiah@gmail.com', 'isiah', '48293ec047687fd5d29089abec85758123a75201', 'male', '0000-00-00', '87810953', 1, '2015-12-03 01:10:14', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
