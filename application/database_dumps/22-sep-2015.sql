-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE DATABASE "msp_app" -------------------------------
CREATE DATABASE IF NOT EXISTS `msp_app` CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `msp_app`;
-- ---------------------------------------------------------


-- CREATE TABLE "courses" ----------------------------------
CREATE TABLE `courses` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`school_id` Int( 11 ) NOT NULL,
	`name` VarChar( 255 ) NOT NULL,
	`created_at` DateTime NOT NULL,
	`updated_at` DateTime NOT NULL,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- ---------------------------------------------------------


-- CREATE TABLE "organisations" ----------------------------
CREATE TABLE `organisations` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`name` Int( 11 ) NOT NULL,
	`created_at` Int( 11 ) NOT NULL,
	`updated_at` Int( 11 ) NOT NULL,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- ---------------------------------------------------------


-- CREATE TABLE "pages_content" ----------------------------
CREATE TABLE `pages_content` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`organisation_id` Int( 11 ) NOT NULL,
	`title` Text NOT NULL,
	`body` Text NOT NULL,
	`created_at` DateTime NOT NULL,
	`updated_at` DateTime NOT NULL,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- ---------------------------------------------------------


-- CREATE TABLE "schools" ----------------------------------
CREATE TABLE `schools` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`name` VarChar( 255 ) NOT NULL,
	`created_at` DateTime NOT NULL,
	`updated_at` DateTime NOT NULL,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- ---------------------------------------------------------


-- CREATE TABLE "student_info" -----------------------------
CREATE TABLE `student_info` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`user_id` Int( 11 ) NOT NULL,
	`school_id` Int( 11 ) NOT NULL,
	`course_id` Int( 11 ) NOT NULL,
	`school_year` Date NOT NULL,
	`created_at` DateTime NOT NULL,
	`updated_at` DateTime NOT NULL,
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- ---------------------------------------------------------


-- CREATE TABLE "users" ------------------------------------
CREATE TABLE `users` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`uid` VarChar( 255 ) NOT NULL,
	`organisation_id` Int( 11 ) NOT NULL,
	`first_name` VarChar( 255 ) NOT NULL,
	`last_name` VarChar( 255 ) NOT NULL,
	`email` VarChar( 255 ) NOT NULL,
	`gender` VarChar( 255 ) NOT NULL,
	`username` VarChar( 255 ) NOT NULL,
	`password` VarChar( 255 ) NOT NULL,
	`birth_date` Date NOT NULL,
	`tel_number` Int( 255 ) NOT NULL,
	`created_at` DateTime NOT NULL,
	`updated_at` DateTime NOT NULL,
	`active` Int( 11 ) NOT NULL DEFAULT '1',
	PRIMARY KEY ( `id` ) )
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- ---------------------------------------------------------


-- Dump data of "courses" ----------------------------------
-- ---------------------------------------------------------


-- Dump data of "organisations" ----------------------------
-- ---------------------------------------------------------


-- Dump data of "pages_content" ----------------------------
-- ---------------------------------------------------------


-- Dump data of "schools" ----------------------------------
-- ---------------------------------------------------------


-- Dump data of "student_info" -----------------------------
-- ---------------------------------------------------------


-- Dump data of "users" ------------------------------------
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


