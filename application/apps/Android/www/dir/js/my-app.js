// Init App
var myApp = new Framework7({
    modalTitle: 'MSP',
    material: true,
    materialRipple: false,
    modalButtonCancel: 'Annuleren',
});

var $$ = Dom7;
 

var mainView = myApp.addView('.view-main');

var deviceReady = false;

// add zero function for date, time and seconds
function zero(val){
	var value;
    if (val < 10) {
      value = '0'+ val;
    }else{
      value = val;
    }
    return value;
  };

function ucFirstAllWords(str){
    var pieces = str.split(" ");
    for ( var i = 0; i < pieces.length; i++ )
    {
        var j = pieces[i].charAt(0).toUpperCase();
        pieces[i] = j + pieces[i].substr(1).toLowerCase();
    }
    return pieces.join(" ");
}

function allStringObject(object){
	var tags = {};

	Object.keys(object).forEach(function(key) {
		if (object[key].toString() != "") {
			tags[key] = object[key].toString();
		}else{
			tags[key] = "off";
		}
		
	});

	return tags;
}

//notification settings data
var notification_settings = {
    'msp': ['on'],
    'school': ['on'],
    'ads': ['on']
  }
//localstorage notification settings check
if (localStorage.getItem('notification_settings') == null) {
	localStorage.setItem('notification_settings', JSON.stringify(notification_settings));
}else if (localStorage.getItem('notification_settings') != null) {
	notification_settings = JSON.parse(localStorage.getItem('notification_settings'));
}


// deviceready function for cordova
document.addEventListener("deviceready", onDeviceReady, false);

// do something after cordova ready
function onDeviceReady() {
	console.log('deviceready');
	deviceReady = true;

	// Enable to debug issues.
	// window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});
	// var notificationReceivedCallback = function(jsonData) {
	//   console.log('notificationReceivedCallback: ' + JSON.stringify(jsonData));
	// };

	var notificationOpenedCallback = function(jsonData) {
	  // console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));

	  // console.log(jsonData);
	  // console.log(jsonData.notification.payload);
	  // console.log(jsonData.notification.payload.additionalData);

	  var type = jsonData.notification.payload.additionalData.type,
	  	  post = jsonData.notification.payload.additionalData.post,
	  	  school_nav = 'scholenstartpagina';

	  if (post == 'niews') {
  		school_nav = 'scholenstartpagina';
	  }else if (post == 'event') {
	  	school_nav = 'school_events';
	  }else if (post == 'rooster') {
	  	school_nav = 'school_rooster';
	  }

	  switch(type){
	  	case 'msp':
	  	setTimeout(function() {
	  		mainView.router.loadPage('views/msp_nieuws.html');
	  	}, 500);
	  	break;

	  	// case 'bos':
	  	// setTimeout(function() {
	  	// 	mainView.router.loadPage('views/bos_nieuws.html');
	  	// }, 500);
	  	// break;

	  	case 'school':
	  	setTimeout(function() {
	  		mainView.router.loadPage('views/'+school_nav+'.html');
	  	}, 500);
	  	break;

	  	case 'ads':
	  	setTimeout(function() {
	  		mainView.router.loadPage('views/advertentie.html');
	  	}, 500);
	  	break;
	  }
	};

	window.plugins.OneSignal
	  .startInit("ec21097b-8c74-4da3-8490-c608a411258e", "535894456358")
	  .handleNotificationOpened(notificationOpenedCallback)
	  .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.InAppAlert)
	  .endInit();

	window.plugins.OneSignal.getIds(function(ids) {
	  console.log('getIds: ' + JSON.stringify(ids));
	});

	// window.plugins.OneSignal.sendTag("key", "value");

	// account tags

	// check if userid key is available
	if(window.localStorage.getItem("userid") !== null){
		window.plugins.OneSignal.setSubscription(true);
		window.plugins.OneSignal.sendTags({"school_id": window.localStorage.getItem("school_id")});
	}else{
		window.plugins.OneSignal.setSubscription(false);
		window.plugins.OneSignal.sendTags({"school_id": "0"});
	}

	if(window.localStorage.getItem("registered") == "yes"){
		window.plugins.OneSignal.sendTags({"private": "true"});
	}else{
		window.plugins.OneSignal.sendTags({"private": "false"});
	}


	// settings tags
	window.plugins.OneSignal.sendTags(allStringObject(notification_settings));

	window.plugins.OneSignal.getTags(function(tags) {
	  console.log('Tags Received: ' + JSON.stringify(tags));
	});

	
	// window.plugins.OneSignal.deleteTag("key");
	// window.plugins.OneSignal.deleteTags(["key1", "key2"]);
	  
	// Sync hashed email if you have a login system or collect it.
	//   Will be used to reach the user at the most optimal time of day.
	// window.plugins.OneSignal.syncHashedEmail(userEmail);

	// You can call this method with false to opt users out of receiving 
	// all notifications through OneSignal. You can pass true later to opt 
	// users back into notifications.
	
	

	// Will show popup dialogs when the devices registers with Apple/Google and with OneSignal.
	// Errors will be shown as popups too if there are any issues.
	// window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});
}

// input focus above keyboard
$$(document).click('input', function(){
	var top = $$(this).offset().top;

	if (top > 300) {
		$$('.page-content').scrollTop(top-100, 600);
	}
})

// image error catch
function replaceImg(){
	"use strict";
	this.src="dir/img/missing.png";
}

$$(document).on('ready', function () {
    $$('img').on("error", replaceImg);
});

// date van vandaag halen
var date = new Date();

//Date voor "Gepost door" -> Global variable

var day = date.getDate();
var month = date.getMonth() + 1;
var year = date.getFullYear();

if (month < 10) month = "0" + month;
if (day < 10) day = "0" + day;
// date omzetten in normale datum (00-00-0000)
var today = year + "-" + month + "-" + day;

// variable voor school menu (over, events, rooster, etc.)
var school_menu_id;

// check if userid key is available
if(window.localStorage.getItem("userid") !== null){
	// close login modal

	myApp.closeModal(".login-screen");
	mainView.router.loadPage('views/msp_nieuws.html');
}

$$('#login_error').hide();
// login screen auth
$$("#login_form").submit(function(event){
	event.preventDefault();

	myApp.showIndicator();

	var username = $$("#login_form").find("#username").val(),
		password = $$("#login_form").find("#password").val();
	// myApp.alert("You clicked login!");
	$$.post("http://miskoropasie.sr/msp_api/index.php/user/login/",{
		username: username,
		password: password
	}, function(data){
		data = JSON.parse(data);
		if (data == 0) {
			$$('#login_error').show().text("Gebruikersnaam of wachtwoord incorrect");
			myApp.hideIndicator();
		} else if (data != 0) {
			myApp.hideIndicator();
			
			if (deviceReady) {
				window.plugins.OneSignal.setSubscription(true);
				window.plugins.OneSignal.sendTags({"school_id": data.school_id});
			}

			$$("#login_form").find("#username").val(""),
			$$("#login_form").find("#password").val("");

			localStorage.setItem("userid", data.userid);
			localStorage.setItem("school_id", data.school_id);
			localStorage.setItem("user_type", data.user_type);
			localStorage.setItem("registered", data.registered);

			// close login modal
			myApp.closeModal(".login-screen");
			mainView.router.loadPage('views/msp_nieuws.html');
		}
	});
});

$$("#logout").click(function(){

    myApp.confirm('Bent u zeker?', 
      function () {
        // myApp.alert("Logout clicked!");
        if (deviceReady) {
			window.plugins.OneSignal.setSubscription(false);
		}
		// remove all localstorages
		localStorage.removeItem("userid");
		localStorage.removeItem("school_id");
		localStorage.removeItem("user_type");
		localStorage.removeItem("registered");

		// back to login
		mainView.router.loadPage('index.html');
		myApp.loginScreen(".login-screen");
      },
      function () {
        // Nothing to do here
      }
    );
	
});

function openNieuws(id, image, date, topic, description, auteur){
	var nieuws_popup_titel = $$("#nieuws_popup_titel");
	var nieuws_popup_image = $$("#nieuws_popup_image");
	var nieuws_popup_post = $$("#nieuws_popup_post");
	var nieuws_popup_topic = $$("#nieuws_popup_topic");
	var nieuws_popup_description = $$("#nieuws_popup_description");
	var nieuws_popup_auteur = $$("#nieuws_popup_auteur");

	nieuws_popup_titel.html("Nieuws");
	nieuws_popup_image.attr("src", image).on('error', replaceImg);
	nieuws_popup_post.text("Gepost op: "+ date);
	nieuws_popup_topic.text(topic);
	nieuws_popup_description.html(description);
	nieuws_popup_auteur.text(auteur);

	myApp.popup('.popup-nieuws');
	myApp.hideIndicator();
}

function openAdvertentie(title, image, description, link){
	var advertentie_popup_titel = $$("#advertentie_popup_titel");
	var advertentie_popup_image = $$("#advertentie_popup_image");
	var advertentie_popup_description = $$("#advertentie_popup_description");
	var advertentie_popup_link = $$("#advertentie_popup_link");

	advertentie_popup_titel.html(title);
	advertentie_popup_image.attr("src", image).on('error', replaceImg);
	advertentie_popup_description.text(description);
	advertentie_popup_link.attr("onclick", "advertisement_link(\'http://www."+link+"\')").html(link);

	myApp.popup('.popup-advertentie');
	myApp.hideIndicator();
}


myApp.onPageBeforeAnimation('index', function (page) {
    myApp.loginScreen();
});

/* ===== msp_nieuws Page ===== */
myApp.onPageInit('msp_nieuws', function (page) {

	// msp_nieuws slider
	var mySwiper1 = myApp.swiper('.swiper_msp_nieuws', {
		speed: 800,
		autoplay: 3000,
		onlyExternal: true
	});

	myApp.showIndicator();

	$$.get("http://miskoropasie.sr/msp_api/index.php/msp/posts",
		function(data){
			var data = JSON.parse(data);
			$$.each(data, function(i, value){
				var description = $$(data[i].description).text();

				$$("#msp_nieuws_all").append('<li class="msp_nieuws_item" data-item-id="'+ data[i].msp_post_id +'">'+
					'<div class="item-content">'+
						'<div class="item-inner">'+
							'<div class="item-title-row">'+
								'<div class="item-title">'+ data[i].topic +'</div>'+
							'</div>'+
							'<div class="nieuws_text item-text">'+ description +'</div>'+
						'</div>'+
					'</div>'+
				'</li>')
			})
			myApp.hideIndicator();
		});

	$$(document).on('click', '.msp_nieuws_item', function (e) {
		myApp.showIndicator();
		// hier haal ik de id attribuut avn de nieuws item
		var id = $$(this).attr("data-item-id");
		// nieuws popup wordt gemaaakt met de id erin

		//Assign msp posts variables
		var topic;
		var description;
		var author;
		var image;

		$$.get('http://miskoropasie.sr/msp_api/index.php/msp/posts_app/'+id, function(data, success) {
			var jsonData = JSON.parse(data);
			
			$$.each(jsonData, function(i, value) {

				/* ----- */

                //Date format(year-month-day)
                var str = jsonData[i].created_at.toString();
                var y0 = str[0];
                var y1 = str[1];
                var y2 = str[2];
                var y3 = str[3];

                var m1 = str[5].toString();
                var m2 = str[6].toString();

                var d1 = str[8].toString();
                var d2 = str[9].toString();

                var monthNames = [
                  "Januari", "Februari", "Maart",
                  "April", "Mei", "Juni", "Juli",
                  "Augustus", "September", "Oktober",
                  "November", "December"
                ];

                var day = d1+d2;
                if(str[5] == 0) {
                    var monthIndex = m2;        
                } else {
                    var monthIndex = m1+m2;
                }
                --monthIndex;

                var year = y0+y1+y2+y3;
                posted_date = day+' '+monthNames[monthIndex]+' '+year;

                /* ------ */

				topic = value.topic;
				description = value.description;
				author = value.firstname + ' ' + value.lastname;
				image = value.img_path + value.img;

				
			});
			
			openNieuws(id, image, posted_date, topic, description, author);
		});
	});

});

myApp.onPageInit('aanmelden', function (page) {

	$$.get("http://miskoropasie.sr/msp_api/index.php/msp/app/scholen/",
	function(data){
		var data = JSON.parse(data);
		// console.log(data);
		$$.each(data, function(i, value){
			var school = '<option value="'+data[i].school_id+'">'+ucFirstAllWords(data[i].name)+'</option>';

			$$('#school').append(school);
		});
	});

	$$('#aanmelden_error, #pass_error, #passconf_error').hide();

	// set minimun age for signup, this case 12
	var d = new Date(),
		y = d.getFullYear() - 12,
		m = zero((d.getMonth() + 1)),
		day = zero(d.getDate());

	d.setFullYear(y);
	var dformat = y+'-'+m+'-'+day;
	$$('#birth_date').attr('max', dformat);


	var pass = $$('#password'), 
		pass_conf = $$('#conf_password'), 
		name = $$('.name');
		sub = $$('input[type=submit');

	// allow only letters
	name.on('keyup', function(){
		var transformedInput = $$(this).val().replace(/[^a-zA-Z\s]/g, '');	

		if (transformedInput !== $$(this).val()) {
			$$(this).val(transformedInput);
		}	
	});

	// password longer than 6 chars
	pass.on('keyup change', function(){
		if (pass.val().length < 6) {
			$$('#pass_error').show();
			sub.addClass('disabled');
		}else{
			$$('#pass_error').hide();
			sub.removeClass('disabled');
		}		
	});	

	// check if same as password
	pass_conf.on('keyup change', function(){
		if (pass.val() != pass_conf.val()) {
			$$('#passconf_error').show();
			sub.addClass('disabled');
		}else{
			$$('#passconf_error').hide();
			sub.removeClass('disabled');
		}	
	});

	$$('#aanmeldenform').submit(function(e){
		e.preventDefault();

		myApp.showIndicator();

		var firstname = $$("#firstname").val(),
			lastname = $$("#lastname").val(),
			email = $$("#email").val(),
			school_id = $$("#school").val(),
			gender = $$("#gender").val(),
			birth_date = $$("#birth_date").val(),
			number = $$("#number").val(),
			password = $$("#password").val();
		
		$$.post("http://miskoropasie.sr/msp_api/index.php/school/users/new_student", {
			firstname: firstname,
			lastname: lastname,
			email: email,
			school_id: school_id,
			gender: gender,
			birth_date: birth_date,
			number: number,
			password: password,
			
		}, function(data){

			switch (data){
		  		case 'exists':
			  		$$('#aanmelden_error').show().text("Email bestaat al");
			  		myApp.hideIndicator();
			  		break;
			  	case 0:
			  		$$('#error').show().text("Er is iets misgegaan, probeer het later opnieuw");
			  		myApp.hideIndicator();
			  		break;
			  	default:
			  		myApp.alert("U staat geregistreerd", function(){
			  			if (deviceReady) {
			  				window.plugins.OneSignal.setSubscription(true);
			  				window.plugins.OneSignal.sendTags({"school_id": school_id});
			  			}

			  			localStorage.setItem("userid", data);
						localStorage.setItem("school_id", school_id);
						localStorage.setItem("user_type", 'student');

						// close login modal
						myApp.closeModal(".login-screen");
						mainView.router.loadPage('views/msp_nieuws.html');
			  		});  		
		  	}
		});
	});

});

myApp.onPageInit('scholen', function () {
	myApp.showIndicator();

	$$.get('http://miskoropasie.sr/msp_api/index.php/msp/scholen/all', function(data) {
		var jsonData = JSON.parse(data);
		
		$$.each(jsonData, function(i, value) {
			var background_class = jsonData[i].app_background_color;

			var content = '\
				<div class="swiper-slide">\
            	<div style="background-color: '+ background_class +';">\
            		<a href="views/scholenstartpagina.html" class="school_menu_id" onclick="school_id_stock(this)" id="'+jsonData[i].school_id+'">\
            			<img src="'+jsonData[i].logo+'" class="scholen-logo" onerror="replaceImg()">\
            		</a>\
            	</div>\
            	<div class="scholen_naam">\
        			<h4>'+jsonData[i].name+'</h4>\
                	<h4>'+jsonData[i].description+'</h4>\
        		</div>\
            	<div class="list-block">\
            		<ul>\
            			<li class="item-content">\
				          <div class="item-media"><i class="icon ion-android-pin"></i></div>\
				          <div class="item-inner">\
				            <div class="item-title">'+jsonData[i].adres+'</div>\
				          </div>\
				        </li>\
				        <li class="item-content">\
				          <div class="item-media"><i class="icon ion-android-call"></i></div>\
				          <div class="item-inner">\
				            <div class="item-title">'+jsonData[i].telefoon+'</div>\
				          </div>\
				        </li>\
				        <li class="item-content">\
				          <div class="item-media"><i class="icon ion-android-mail"></i></div>\
				          <div class="item-inner">\
				            <div class="item-title">'+jsonData[i].email+'</div>\
				          </div>\
				        </li>\
				        <li class="item-content">\
				          <div class="item-media"><i class="icon ion-android-globe"></i></div>\				          <div class="item-inner">\
				            <div class="item-title">'+jsonData[i].website+'</div>\
				          </div>\
				        </li>\
            		</ul>\
                </div>\
            </div>\
			';

			$$('.scholenAll').append(content);
		});
		myApp.hideIndicator();
	});
});

myApp.onPageAfterAnimation('scholen', function (page) {
	// scholen slider
	var mySwiper2 = myApp.swiper('.swiper_scholen', {
		speed: 300,
        loop: true,
        pagination:'.swiper-pagination'
	});

	$$('.school_menu_id').click(function(){
		school_menu_id = $$(this).attr('id');
		// console.log(school_menu_id);
	});
});

myApp.onPageInit('mijn_profiel', function (page) {
	myApp.showIndicator();

	$$.get("http://miskoropasie.sr/msp_api/index.php/msp/app/scholen/",
	function(data){
		var data = JSON.parse(data);
		// console.log(data);
		$$.each(data, function(i, value){
			var school = '<option value="'+data[i].school_id+'">'+ucFirstAllWords(data[i].name)+'</option>';

			$$('#school').append(school);
		});
	});

	var user_id = localStorage.getItem("userid");
	$$.get("http://miskoropasie.sr/msp_api/index.php/school/user/" + user_id,
		function(data){
			var data = JSON.parse(data);
			console.log(data);
			$$.each(data, function(i, value){
				$$("#firstname").val(data[i].firstname);
				$$("#lastname").val(data[i].lastname);
				$$("#number").val(data[i].number);
				$$("#email").val(data[i].email);
				$$("#school").val(data[i].school_id);
			});
			myApp.hideIndicator();
		});

	$$('#mijn_profiel_edit').on('submit', function (event) {
		event.preventDefault();
		myApp.confirm('Wijzigingen opslaan?', 
		// bij het bevestigen wordt dit uitgevoerd
		  function () {
			$$.post("http://miskoropasie.sr/msp_api/index.php/school/users/edit", {
				user_id: user_id,
				firstname: $$("#firstname").val(),
				lastname: $$("#lastname").val(),
				number: $$("#number").val(),
				email: $$("#email").val(),
				school_id: $$("#school").val()
			}, function(data){
				if (data == 1) {
					myApp.alert('Opgeslagen');
				}else{
					myApp.alert('er is iets misgegaan, probeer het later opnieuw');
				}
				
			});
		  },
		  // bij het annuleren wordt dit uitgevoerd
		  function () {
		  }
		);
	});

	$$('#account_sluiten').on('click', function () {
		myApp.confirm('Uw account kunt u hierna niet herstellen, doorgaan?',

		// bij het bevestigen wordt dit uitgevoerd 
		  function () {
			myApp.alert('Account gesloten', function (){
				if (deviceReady) {
	  				window.plugins.OneSignal.setSubscription(false);
	  				window.plugins.OneSignal.sendTags({"school_id": "0"});
	  			}
				$$.post('http://miskoropasie.sr/msp_api/index.php/school/users/delete/'+localStorage.getItem('userid'), function(data) {
					if(data == 1) {
						mainView.router.loadPage('index.html');
					}
				});
			})
		  },
		  // bij het annuleren wordt dit uitgevoerd
		  function () {
		  	
		  }
		);
	});
});

myApp.onPageInit('student_verificatie', function (page) {

	if(window.localStorage.getItem("registered") == "yes"){
		$$('.activeer').hide();
		$$('.geactiveerd').show();
	}

	$$('#verifieer').on('click', function () {
		myApp.showIndicator();
		$$.post('http://miskoropasie.sr/msp_api/index.php/school/users/verification/'+localStorage.getItem('userid'), 
		{
			code: $$('#verification_code').val(),
			school_id: localStorage.getItem('school_id')
		}, function(data) {
			var data = JSON.parse(data);
			if(data == 1) {
				if (deviceReady) {
	  				window.plugins.OneSignal.sendTags({"private": "true"});
	  			}
				myApp.alert('Verificatie succesvol.', function () {
					localStorage.setItem("registered", "yes");
					mainView.router.back();
				});
			} else if(data.result == "no") {
				myApp.alert('Verificatie mislukt of code is foutief.', function () {
					// mainView.router.back();
				});
			} else if(data == 200) {
				myApp.alert('U heeft de schoolcode al geverifieerd.', function () {
					// mainView.router.back();
				});
			}
			myApp.hideIndicator();
		});

		// if (succesvol) {
		//     myApp.alert('Verificatie succesvol');
		// }else{
		//     myApp.alert('Verificatie mislukt');
		// }; 
	}); 
});

myApp.onPageInit('advertentie', function (page) {
	myApp.showIndicator();
	$$.get('http://miskoropasie.sr/msp_api/index.php/msp/ads', function(data) {
		var jsonData = JSON.parse(data);
		$$.each(jsonData, function(i, value) {
			var content = '\
				<div class="card deal_item" data-item-id="'+jsonData[i].ad_id+'">\
		          <div class="card-content advertentie">\
		            <p class="text_center">'+jsonData[i].title+'</p>\
		            <img src="'+jsonData[i].img_path+jsonData[i].img+'" width="100%" height="auto" onerror="replaceImg()">\
		          </div>\
		        </div>\
			';

			$$('#advertenties').append(content);
		})
		myApp.hideIndicator();
	});

	$$(document).on('click', '.deal_item', function (e) {
		myApp.showIndicator();
		// hier haal ik de id attribuut avn de nieuws item
		var id = $$(this).attr("data-item-id");
		// var img_src = $$(this).find('img').attr('src');

		//Assign content variables
		var title;
		var description;
		var ad_img;

		$$.get('http://miskoropasie.sr/msp_api/index.php/msp/ads/'+id, function(data) {
			var jsonData = JSON.parse(data);
			$$.each(jsonData, function(i, value) {
				title = value.title;
				description = value.description;
				ad_img = value.img_path + value.img;
				ad_link = value.link;
			});

			openAdvertentie(title, ad_img, description, ad_link);

		});
	});
});

myApp.onPageInit('notifications', function (page) {
	myApp.formFromJSON('#notifications', JSON.parse(localStorage.getItem('notification_settings')));
	
	$$('#opslaan').on('click', function () {
		var formData = myApp.formToJSON('#notifications');
  		console.log(JSON.stringify(formData));
  		localStorage.setItem('notification_settings', JSON.stringify(formData));
  		if (deviceReady) {
  			window.plugins.OneSignal.sendTags(allStringObject(formData));
  		}
		myApp.alert('Opgeslagen', function () {
			mainView.router.back();
		  });
	});
});

// myApp.onPageInit('bos', function (page) {
// 	// bos slider
// 	var mySwiper1 = myApp.swiper('.swiper_bos', {
// 		speed: 800,
// 		autoplay: 3000,
// 		onlyExternal: true
// 	});

// 	$$.get('http://miskoropasie.sr/msp_api/index.php/bos/contact', function(data, success) {
//               var responseData = JSON.parse(data);
//               $$.each(responseData, function(i, value){
                
//                 var number;
//                 if(responseData[i].number_1 != "" && responseData[i].number_2 != "") {
//                   number = responseData[i].number_1+' of '+responseData[i].number_2;
//                 } else if(responseData[i].number_1 == "" && responseData[i].number_2 != "") {
//                   number = responseData[i].number_2;
//                 } else if(responseData[i].number_1 != "" && responseData[i].number_2 == "") {
//                   number = responseData[i].number_1;
//                 } else if(responseData[i].number_1 == "" && responseData[i].number_2 == "") {
//                   number = 'N/A';
//                 } 

//                 var content = '\
//                 	<div class="list-block">\
//                         <ul>\
//                           <li class="item-content">\
//                           <div class="item-media"><i class="icon ion-android-pin"></i></div>\
//                           <div class="item-inner">\
//                             <div class="item-title">'+responseData[i].address+'</div>\
//                           </div>\
//                         </li>\
//                         <li class="item-content">\
//                           <div class="item-media"><i class="icon ion-android-call"></i></div>\
//                           <div class="item-inner">\
//                             <div class="item-title">'+number+'</div>\
//                           </div>\
//                         </li>\
//                         <li class="item-content">\
//                           <div class="item-media"><i class="icon ion-android-mail"></i></div>\
//                           <div class="item-inner">\
//                             <div class="item-title">'+responseData[i].email+'</div>\
//                           </div>\
//                         </li>\
//                         <li class="item-content">\
//                           <div class="item-media"><i class="icon ion-android-globe"></i></div>\
//                           <div class="item-inner">\
//                             <div class="item-title">'+responseData[i].website+'</div>\
//                           </div>\
//                         </li>\
//                         </ul>\
//                       </div>\
//                 ';

//                 $$('#bos_contact').append(content);

//               })
//           });
// });
///////////////////////////////////////////////

// bos_nieuws page
// myApp.onPageBeforeAnimation('bos_nieuws', function (page) {
// 	// bos_nieuws slider
// 	var mySwiper4 = myApp.swiper('.swiper_bos_nieuws', {
// 		speed: 800,
// 		autoplay: 3000,
// 		onlyExternal: true
// 	});

// 	myApp.showIndicator();

// 	$$.get('http://miskoropasie.sr/msp_api/index.php/bos/posts', function(data, success) {
//         var responseData = JSON.parse(data);
//         // console.log(responseData);
//         $$.each(responseData, function(i, value) {
//           if(responseData[i].updated_at == '0000-00-00 00:00:00' || responseData[i].updated_at == null) {
//               var updated = "N/A";
//               var date_time = "----";
//             } else {
//               var updated = responseData[i].updated_at;
//               var date_time = responseData[i].updated_at;
//             }

//             //Shorten description content if content is too long
//             var description = responseData[i].description;
//             var num = description.length;
//             var max_length = 150;
//             if(num > max_length) {
//               description = description.slice(0, max_length) + '...';
//             }

//             var content = '\
//                 <li class="nieuws_item" data-item-id="'+responseData[i].bos_post_id+'">\
//                     <div class="item-content">\
//                         <div class="item-media">\
//                             <img width="auto" height="80px" src="'+responseData[i].img_path+responseData[i].img+'" onerror="replaceImg()">\
//                         </div>\
//                         <div class="item-inner">\
//                             <div class="item-title-row">\
//                                 <div class="item-title">'+responseData[i].topic+'</div>\
//                             </div>\
//                             <div class="item-text">'+description+'</div>\
//                         </div>\
//                     </div>\
//                 </li>\
//             ';

//             $$('#bos_posts').append(content);
            
//         });
//         myApp.hideIndicator();
//       });

// 	$$(document).on('click', '.nieuws_item', function (e) {
// 		myApp.showIndicator();
// 		// hier haal ik de id attribuut avn de nieuws item
// 		var id = $$(this).attr("data-item-id");

// 		//Assign msp posts variables
// 		var topic;
// 		var description;
// 		var image;

// 		$$.get('http://miskoropasie.sr/msp_api/index.php/bos/posts/'+id, function(data, success) {
// 			var jsonData = JSON.parse(data);

// 			$$.each(jsonData, function(i, value) {

// 				/* ----- */

//                 //Date format(year-month-day)
//                 var str = jsonData[i].created_at.toString();
//                 var y0 = str[0];
//                 var y1 = str[1];
//                 var y2 = str[2];
//                 var y3 = str[3];

//                 var m1 = str[5].toString();
//                 var m2 = str[6].toString();

//                 var d1 = str[8].toString();
//                 var d2 = str[9].toString();

//                 var monthNames = [
//                   "Januari", "Februari", "Maart",
//                   "April", "Mei", "Juni", "Juli",
//                   "Augustus", "September", "Oktober",
//                   "November", "December"
//                 ];

//                 var day = d1+d2;
//                 if(str[5] == 0) {
//                     var monthIndex = m2;        
//                 } else {
//                     var monthIndex = m1+m2;
//                 }
//                 --monthIndex;

//                 var year = y0+y1+y2+y3;
//                 posted_date = day+' '+monthNames[monthIndex]+' '+year;

//                 /* ------ */

// 				topic = value.topic;
// 				description = $$(value.description).text();
// 				image = value.img_path + value.img;
// 				auteur = "";
// 			});

// 			openNieuws(id, image, posted_date, topic, description, auteur);
// 		});
// 		// console.log(content);
// 	});
// });

// myApp.onPageInit('studietoelage_aanvraag', function (page) {
// 	// omgezette datum als value zetten bij geboorte datum
// 	$$('#geboortedatum').val(today);
// 	var user_id = localStorage.getItem("userid");
// 	$$.get("http://miskoropasie.sr/msp_api/index.php/school/user/" + user_id,
// 		function(data){
// 			var data = JSON.parse(data);
// 			// console.log(data);
// 			$$.each(data, function(i, value){
// 				$$("#voornaam").val(data[i].firstname);
// 				$$("#achternaam").val(data[i].lastname);
//             		$$("#email").val(data[i].email);
//             		$$("#nummer").val(data[i].nummer);
//             		$$("#nationaliteit").val(data[i].nationaliteit);
//             		$$("#klas").val(data[i].klas);
// 			});
// 		});

// 	$$('#versturen').on('click', function () {
//             var voornaam = $$("#voornaam").val();
//             var achternaam = $$("#achternaam").val();
//             var geboortedatum = $$("#geboortedatum").val();
//             var geslacht = $$("#geslacht").val();
//             var nationaliteit = $$("#nationaliteit").val();
//             var school = $$("#school").val();
//             var klas = $$("#klas").val();
//             var nummmer = $$("#nummmer").val();
//             var email = $$("#email").val();

//             $$.post("http://miskoropasie.sr/msp_api/index.php/aanvraag/", {
//             	voornaam: voornaam,
//             	achternaam: achternaam,
//             	geboortedatum: geboortedatum,
//             	geslacht: geslacht,
//             	nationaliteit: nationaliteit,
//             	school: school,
//             	klas: klas,
//             	nummmer: nummmer,
//             	email: email
//             }, function(data){
//             	if (data) {
//             		myApp.alert("Bedankt voor uw aanvraag");
//             		mainView.router.back();
//             	} else {
//             		myApp.alert("Niet gelukt");
//             	}
//             });
//     });
// });

// myApp.onPageInit('studiebeurs_aanvraag', function (page) {
// 	// omgezette datum als value zetten bij geboorte datum
// 	var user_id = localStorage.getItem("userid");
// 	$$.get("http://miskoropasie.sr/msp_api/index.php/school/user/" + user_id,
// 		function(data){
// 			var data = JSON.parse(data);
// 			// console.log(data);
// 			$$.each(data, function(i, value){
// 				$$("#voornaam").val(data[i].firstname);
// 				$$("#achternaam").val(data[i].lastname);
//             		$$("#email").val(data[i].email);
//             		$$("#nummer").val(data[i].nummer);
//             		$$("#nationaliteit").val(data[i].nationaliteit);
//             		$$("#klas").val(data[i].klas);
// 			});
// 		});
// 	$$('#geboortedatum').val(today);

// 	$$('#versturen').on('click', function () {
//             // myApp.alert('Verstuurd', function () {
//             //     mainView.router.back();
//             //   });
//             var voornaam = $$("#voornaam").val();
//             var achternaam = $$("#achternaam").val();
//             var geboortedatum = $$("#geboortedatum").val();
//             var geslacht = $$("#geslacht").val();
//             var nationaliteit = $$("#nationaliteit").val();
//             var school = $$("#school").val();
//             var klas = $$("#klas").val();
//             var nummmer = $$("#nummmer").val();
//             var email = $$("#email").val();

//             // console.log(voornaam + achternaam);

//             $$.post("http://miskoropasie.sr/msp_api/index.php/aanvraag/", {
//             	voornaam: voornaam,
//             	achternaam: achternaam,
//             	geboortedatum: geboortedatum,
//             	geslacht: geslacht,
//             	nationaliteit: nationaliteit,
//             	school: school,
//             	klas: klas,
//             	nummmer: nummmer,
//             	email: email
//             }, function(data){
//             	if (data) {
//             		myApp.alert("Bedankt voor uw aanvraag");
//             		mainView.router.back();
//             	} else {
//             		myApp.alert("Niet gelukt");
//             	}
//             });
//     });
// });

// myApp.onPageInit('studievervoer_aanvraag', function (page) {
// 	var user_id = localStorage.getItem("userid");
// 	$$.get("http://miskoropasie.sr/msp_api/index.php/school/user/" + user_id,
// 		function(data){
// 			var data = JSON.parse(data);
// 			// console.log(data);
// 			$$.each(data, function(i, value){
// 				$$("#voornaam").val(data[i].firstname);
// 				$$("#achternaam").val(data[i].lastname);
//             		$$("#email").val(data[i].email);
//             		$$("#nummer").val(data[i].nummer);
//             		$$("#nationaliteit").val(data[i].nationaliteit);
//             		$$("#klas").val(data[i].klas);
// 			});
// 		});

// 	$$('#versturen').on('click', function () {

//             var voornaam = $$("#voornaam").val();
//             var achternaam = $$("#achternaam").val();
//             var geboortedatum = $$("#geboortedatum").val();
//             var geslacht = $$("#geslacht").val();
//             var nationaliteit = $$("#nationaliteit").val();
//             var school = $$("#school").val();
//             var klas = $$("#klas").val();
//             var nummmer = $$("#nummmer").val();
//             var email = $$("#email").val();

//             // console.log(voornaam + achternaam);

//             $$.post("http://miskoropasie.sr/msp_api/index.php/aanvraag/", {
//             	voornaam: voornaam,
//             	achternaam: achternaam,
//             	geboortedatum: geboortedatum,
//             	geslacht: geslacht,
//             	nationaliteit: nationaliteit,
//             	school: school,
//             	klas: klas,
//             	nummmer: nummmer,
//             	email: email
//             }, function(data){
//             	if (data) {
//             		myApp.alert("Bedankt voor uw aanvraag");
//             		mainView.router.back();
//             	} else {
//             		myApp.alert("Niet gelukt");
//             	}
//             });
//     });
// });

myApp.onPageInit('scholen_startpagina', function (page) {
	// scholen_startpagina slider
	var mySwiper5 = myApp.swiper('.swiper_scholen_startpagina', {
		speed: 800,
		autoplay: 2000,
		onlyExternal: true,
		slidesPerView: 2,
		spaceBetween: 1
	});

	if(localStorage.getItem("registered") == "yes" && localStorage.getItem("school_id") == school_menu_id) {
		// console.log('private');
		$$.get("http://miskoropasie.sr/msp_api/index.php/school/posts/private/" + school_menu_id,
		function(data) {
			var data = JSON.parse(data);
			$$.each(data, function(i, value) {
				var description = $$(data[i].description).text();

				$$("#scholen_nieuws").append('<li class="nieuws_item" data-item-id="'+ data[i].school_post_id +'">'+
					'<div class="item-content">'+
						'<div class="item-inner">'+
							'<div class="item-title-row">'+
								'<div class="item-title">'+ data[i].topic +'</div>'+
							'</div>'+
							'<div class="nieuws_text item-text">'+ description +'</div>'+
						'</div>'+
					'</div>'+
				'</li>')
				})
		});
		myApp.hideIndicator();
	} else {
		// console.log('public');
		$$.get("http://miskoropasie.sr/msp_api/index.php/school/posts/public/" + school_menu_id,
		function(data) {
			var data = JSON.parse(data);
			console.log(data);
			$$.each(data, function(i, value) {
				var description = $$(data[i].description).text();

				$$("#scholen_nieuws").append('<li class="nieuws_item" data-item-id="'+ data[i].school_post_id +'">'+
					'<div class="item-content">'+
						'<div class="item-inner">'+
							'<div class="item-title-row">'+
								'<div class="item-title">'+ data[i].topic +'</div>'+
							'</div>'+
							'<div class="nieuws_text item-text">'+ description +'</div>'+
						'</div>'+
					'</div>'+
				'</li>')
				})
		});
		myApp.hideIndicator();
	}

	$$(document).on('click', '.nieuws_item', 
		function (e) {
		myApp.showIndicator();
		// hier haal ik de id attribuut avn de nieuws item
		var id = $$(this).attr("data-item-id");
		// nieuws popup wordt gemaaakt met de id erin

		//Assign msp posts variables
		var topic;
		var description;
		var author;
		var image;

		$$.get('http://miskoropasie.sr/msp_api/index.php/school/posts_app/id/' + id, 
			function(data, success) {
			var jsonData = JSON.parse(data);
			
			$$.each(jsonData, function(i, value) {

				/* ----- */

                //Date format(year-month-day)
                var str = jsonData[i].created_at.toString();
                var y0 = str[0];
                var y1 = str[1];
                var y2 = str[2];
                var y3 = str[3];

                var m1 = str[5].toString();
                var m2 = str[6].toString();

                var d1 = str[8].toString();
                var d2 = str[9].toString();

                var monthNames = [
                  "January", "February", "March",
                  "April", "May", "June", "July",
                  "August", "September", "October",
                  "November", "December"
                ];

                var day = d1+d2;
                if(str[5] == 0) {
                    var monthIndex = m2;        
                } else {
                    var monthIndex = m1+m2;
                }
                --monthIndex;

                var year = y0+y1+y2+y3;
                posted_date = day+' '+monthNames[monthIndex]+' '+year;

                /* ------ */

				topic = value.topic;
				description = $$(value.description).text();
				image = value.img_path + value.img;
			});
			
			openNieuws(id, image, posted_date, topic, description, author);
		});
	});
});

myApp.onPageInit('school_events', function (page) {
	myApp.showIndicator();
	$$.get('http://miskoropasie.sr/msp_api/index.php/school/events/'+school_menu_id, function(data) {
		var jsonData = JSON.parse(data);
		// console.log(jsonData);
		$$.each(jsonData, function(i, value) {

			/* ----- */

			//time format
            var time = jsonData[i].time;
            var cut = time.lastIndexOf(":");
            var newTime = time.slice(0, cut);

            //Date format(year-month-day)
            var str = jsonData[i].date.toString();
            var y0 = str[0];
            var y1 = str[1];
            var y2 = str[2];
            var y3 = str[3];

            var m1 = str[5].toString();
            var m2 = str[6].toString();

            var d1 = str[8].toString();
            var d2 = str[9].toString();

            var monthNames = [
              "January", "February", "March",
              "April", "May", "June", "July",
              "August", "September", "October",
              "November", "December"
            ];

            var day = d1+d2;
            if(str[5] == 0) {
                var monthIndex = m2;        
            } else {
                var monthIndex = m1+m2;
            }
            --monthIndex;

            var year = y0+y1+y2+y3;
            posted_date = day+' '+monthNames[monthIndex]+' '+year;

            /* ------ */

			var content = '\
				<div class="content-block">\
		          <div class="card">\
		            <div class="card-header event_head">\
		              <div class="content-block" style="width:100%; padding:0 !important">\
		                <div class="row">\
		                  <div class="col-100">\
		                    <div class="list-block inset event_list">\
		                      <ul>\
		                        <li class="item-content">\
		                          <div class="item-media"><i class="icon ion-android-calendar"></i></div>\
		                          <div class="item-inner text_uppercase">\
		                            <div class="item-title">'+posted_date+'</div>\
		                          </div>\
		                        </li>\
		                        <li class="item-content">\
		                          <div class="item-media"><i class="icon ion-android-time"></i></div>\
		                          <div class="item-inner">\
		                            <div class="item-title">'+newTime+'</div>\
		                          </div>\
		                        </li>\
		                      </ul>\
		                    </div>\
		                  </div>\
		                </div>\
		              </div>\
		            </div>\
		            <div class="card-content">\
		              <div class="card-content-inner">\
		                <div class="row">\
		                  <div class="col-40">\
		                    <img src="'+jsonData[i].img_path+jsonData[i].img+'" alt="event-image" width="100%" onerror="replaceImg()">\
		                  </div>\
		                  <div class="col-60 text_capitalize">\
		                    <h1>'+jsonData[i].topic+'</h1>\
		                  </div>\
		                </div>\
		                <p class="color-grey">'+jsonData[i].description+'</p>\
		              </div>\
		            </div>\
		            <div class="card-footer event_footer">\
		              <div class="row">\
		                <div class="col-100">\
		                  <div class="list-block color-black inset event_list">\
		                    <ul>\
		                      <li class="item-content">\
		                        <div class="item-media"><i class="icon ion-android-pin"></i></div>\
		                        <div class="item-inner text_capitalize">\
		                          <p>'+jsonData[i].location+'</p>\
		                        </div>\
		                      </li>\
		                    </ul>\
		                  </div>\
		                </div>\
		              </div>\
		            </div>\
		          </div>\
		        </div>\
			';
			$$('#school_events').append(content);
		})

		myApp.hideIndicator();
	})
});

myApp.onPageInit('school_rooster', function (page) {
	myApp.showIndicator();
	$$.get('http://miskoropasie.sr/msp_api/index.php/school/roosters/'+school_menu_id, function(data) {
		var jsonData = JSON.parse(data);
		$$.each(jsonData, function(i, value) {
			var content = '\
				<div class="content-block">\
			        <div class="content-block-inenr">\
			          <p>'+jsonData[i].description+'</p>\
			          <a class="button" link="'+jsonData[i].pdf_path+jsonData[i].uploaded_pdf+'" onclick="openpdf(this)">Open PDF</a>\
			          <hr />\
			        </div>\
			      </div>\
			';
			$$('#school_roosters').append(content);
		})

		myApp.hideIndicator();
	})
});

myApp.onPageInit('school_richting', function (page) {
	var school_id_stock = localStorage.getItem('school_id_stock');
	myApp.showIndicator();
	$$.get('http://miskoropasie.sr/msp_api/index.php/richtingen/school_id='+school_id_stock, function(data) {
		data = JSON.parse(data);
		$$.each(data, function(i, value) {
			var content = '\
	              <li class="accordion-item">\
	                  <a href="" class="item-link item-content bg-green color-white">\
	                      <div class="item-inner">\
	                          <div class="item-title">'+data[i].richting+'</div>\
	                      </div>\
	                  </a>\
	                  <div class="accordion-item-content">\
	                    <div class="richting_img" align="center">\
	                      <img src="'+data[i].img_path+data[i].img+'" alt="'+data[i].richting+' Image" onerror="replaceImg()">\
	                    </div>\
	                    <div class="richting_text">\
	                      <h1>'+data[i].richting+'</h1>\
	                      <p>'+data[i].body+'</p>\
	                    </div>\
	                  </div>\
	              </li>\
	              <br>\
			';

			$$('#richtingen_content').append('<ul>'+content+'</ul>');
		});

		myApp.hideIndicator();
	});
	
});

myApp.onPageInit('school_regelement', function (page) {
	var school_id_stock = localStorage.getItem('school_id_stock');
	myApp.showIndicator();
	$$.get('http://miskoropasie.sr/msp_api/index.php/schoolreglement/'+school_id_stock, function(data) {
		data = JSON.parse(data);
		$$('#reglement_content').append(data[0].description + '<br>');
		myApp.hideIndicator();
	});
});

myApp.onPageInit('school_over', function (page) {
	//Scholen about page
	myApp.showIndicator()
	$$.get('http://miskoropasie.sr/msp_api/index.php/msp/scholen/school_about/'+school_menu_id, function(data) {
		var data = JSON.parse(data);
		console.log(data);
		$$.each(data, function(i, value) {
			var school_about = '<div style="background-color: '+ data[i].app_background_color +';">\
				            		<img src="'+data[i].logo+'" class="scholen-logo" onerror="replaceImg()">\
				            	</div>\
				            	<div class="scholen_naam">\
				        			<h1>'+data[i].name+'</h1>\
				        		</div>\
				        		<div class="content-block">\
				        			<div class="content-block-inner about">\
				        				<h2>Over de school</h2>\
				        				<p>'+$$(data[i].about).text()+'</p>\
				        			</div>\
				        		</div>';

			$$('#school_about').append(school_about);
		})

		myApp.hideIndicator();
	})
});

myApp.onPageInit('faq', function (page) {
	myApp.showIndicator();
	$$.get('http://miskoropasie.sr/msp_api/index.php/msp/faq', function(data) {
		var data = JSON.parse(data);
		$$.each(data, function(i, value) {
			var content = '\
				<ul>\
		          <li class="accordion-item"><a href="#" class="item-content item-link">\
		              <div class="item-inner">\
		                <div class="item-title">'+data[i].question+'</div>\
		              </div></a>\
		            <div class="accordion-item-content">\
		              <div class="content-block">\
		                <p>'+data[i].answer+'</p>\
		              </div>\
		            </div>\
		          </li>\
		        </ul>\
			';
			$$('#faq_content').append(content);
		})

		myApp.hideIndicator();
	})
});

myApp.onPageInit('over_msp', function (page) {
	//Scholen about page
	myApp.showIndicator();
	$$.get('http://miskoropasie.sr/msp_api/index.php/msp/about', function(data) {
		var data = JSON.parse(data);
		$$.each(data, function(i, value) {
			$$('#over_msp').append(data[i].description);
		})

		myApp.hideIndicator();
	})
});

//Function to show school rooster
function openpdf(element) {
	var link = element.getAttribute('link');
	link = encodeURIComponent(link);
	window.location = 'https://docs.google.com/viewer?embedded=true&url='+link;
}

//Function to go to ad link
/* Params -> (url) */
function advertisement_link(url) {
	window.location = url;
};
/*-----------------------------------------*/

/**
* Deze functie is bedoeld om de school_id van te nemen voor school richtingen en niet de school_id van de user
*/
function school_id_stock(obj) {
	var id = obj.getAttribute('id');
	localStorage.setItem('school_id_stock', id);
}
/*-----------------------------------------*/
